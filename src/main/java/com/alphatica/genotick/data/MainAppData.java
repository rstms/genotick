package com.alphatica.genotick.data;

import com.alphatica.genotick.genotick.RobotData;
import com.alphatica.genotick.genotick.TradeMode;
import com.alphatica.genotick.timepoint.TimePoint;
import com.alphatica.genotick.ui.UserInputOutputFactory;
import com.alphatica.genotick.ui.UserOutput;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Collections.binarySearch;

// TODO change nulls to optional
public class MainAppData {

    private final Map<DataSetName, DataSet> sets;
    private final UserOutput output = UserInputOutputFactory.getUserOutput();
    private List<TimePoint> timePoints;

    public MainAppData() {
        sets = new HashMap<>();
        timePoints = new ArrayList<>();
    }

    public void addDataSet(DataSet set) {
        sets.put(set.getName(), set);
        updateTimePoints(set.getTimePoints());
    }

    public void validateReverseFilesPresent() {
        List<String> reverseNames = new ArrayList<>();
        List<String> names = new ArrayList<>();
        for (DataSetName dataSetName: sets.keySet()) {
            if (dataSetName.isReverse()) {
                reverseNames.add(dataSetName.getName());
            } else {
                names.add(dataSetName.getName());
            }
        }
        for (String name: names) {
            String reverse = DataSetName.REVERSE_PREFIX + name;
            if (!reverseNames.contains(reverse)) {
                output.errorMessage("Symmetrical robots required but no reverse data found for " + name + "." +
                "Read about reverse data here: https://genotick.com/help -> `What is 'reverse data' and why do I need this?`");
                throw new IllegalStateException();
            }
        }
    }

    private void updateTimePoints(List<TimePoint> newTimePoints) {
        timePoints.addAll(newTimePoints);
        timePoints = timePoints.stream().distinct().collect(Collectors.toList());
        timePoints.sort(TimePoint::compareTo);
    }

    public List<RobotData> prepareRobotDataList(final TimePoint timePoint) {
        List<RobotData> list = Collections.synchronizedList(new ArrayList<>());
        sets.entrySet().parallelStream().forEach((Map.Entry<DataSetName, DataSet> entry) -> {
            RobotData robotData = entry.getValue().getRobotData(timePoint);
            if (!robotData.isEmpty())
                list.add(robotData);

        });
        return list;
    }

    public TimePoint getFirstTimePoint() {
        if (sets.isEmpty())
            return null;
        return timePoints.get(0);
    }

    public TimePoint getNextTimePint(TimePoint now) {
        int index = binarySearch(timePoints, now);
        if(index < 0) {
            index = Math.abs(index + 1);
        }
        if(index > timePoints.size() - 2) {
            return null;
        } else  {
            return timePoints.get(index+1);
        }
    }

    public TimePoint getLastTimePoint() {
        if (sets.isEmpty())
            return null;
        return timePoints.get(timePoints.size()-1);
    }


    public Collection<DataSet> listSets() {
        return sets.values();
    }

    void validate() {
        if(sets.isEmpty()) {
            throw new DataException("No files to read!");
        }
    }

    public void validateTradeOpenPrice(TradeMode tradeMode) {
        sets.forEach((name, set) -> validateTradeOpenPrice(tradeMode, name, set));
    }

    private void validateTradeOpenPrice(TradeMode tradeMode, DataSetName name, DataSet set) {
        for (int i = 0; i < set.getLinesCount(); i++) {
            Number[] line = set.getLine(i);
            if (line[tradeMode.getColumn()].doubleValue() <= 0) {
                output.errorMessage(String.format("Invalid price (negative or 0) for opening trade. Name: %s, line: %d", name.getName(), i+1));
                throw new DataException("Invalid trade open price " + name.getName() + ":" + (i+1));
            }
        }
    }

}
